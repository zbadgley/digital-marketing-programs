### Digital Marketing Programs Team

*  **Digital Marketing Programs Manager:** [Shane Rice](https://gitlab.com/shanerice)
    * **Focus:** SEO, web analytics 
*  **Digital Marketing Programs Manager:** [Niall Cregan](https://gitlab.com/ncregan)
    * **Focus:** SEO 
*  **Digital Marketing Manager:** [Matt Nguyen](https://gitlab.com/mnguyen4)
    * **Focus:** Digital advertising (paid search, paid social, display advertising, sponsorships)
*  **Digital Marketing Manager:** [Leslie Stinson](https://gitlab.com/lstinson)
    * **Focus:** Digital advertising (acceleration team, ABM, Field Marketing support)

### Marketing Programs Team

*  **Manager, Marketing Programs:** [Jackie Gragnola](https://gitlab.com/jgragnola)
*  **Sr. Marketing Program Manager:** [Agnes Oetama](https://gitlab.com/aoetama)
*  **Sr. Marketing Program Manager:** [Jenny Tiemann](https://gitlab.com/jennyt)
*  **Sr. Marketing Program Manager:** [Zac Badgley](https://gitlab.com/zbadgley)
*  **Sr. Marketing Program Manager:** [Nout Boctor-Smith](https://gitlab.com/nbsmith)
*  **Sr. Marketing Program Manager:** [Megan Mitchell](https://gitlab.com/mmmitchell)
*  **Sr. Marketing Program Manager:** [Eirini Pan](https://gitlab.com/eirinipan)
*  **Marketing Program Manager:** [Indre Kryzeviciene](https://gitlab.com/ikryzeviciene)
