# GATED CONTENT DETAILS

#### Execution Timeline

📦 **Asset Delivery Date: `X`** - must be provided by content owner (date the temporary analyst asset will be provided to GitLab for preparation)  
🚀 **Asset Launch Date: `X`** - must be provided by content owner (15 *business days* after delivery day indicated above)  
📅 **Pushed Live Date: `X`** - MPM to fill in when page is live  
✅ **Aligned Campaign: `X`** - choose from active campaigns listed [here](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1839132097). All content should align to live and upcoming campaigns.  

#### DRIs & Links

* **Content Owner:** 
* **MPM Owner:** 
* **PMM Owner:** 
* **Official Content Name:** `put final name here`
* **Link to Content:** (Upload to PathFactory and link to the asset here)
* **Marketo:** 
* **Salesforce:** 

### [Landing Page Copy Document >>](https://docs.google.com/document/d/1xHnLKPCaXrpEe1ccRh_7-IqgNbAlzQsZVc-wr1W4ng8/edit#) - *make a copy of this template and provide requested copy*

# ACTION ITEMS

## Create landing page
* [ ]  Write copy for landing page - content owner
* [ ]  Provide edits to copy in Copy Document as necessary - MPM
* [ ]  Create page on www-gitlab-com [quick link to repo](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/resources) - MPM
* **While pipeline running, skip to steps below**

## Create program in Marketo
* [ ]  Clone [gated content template Marketo program](https://app-ab13.marketo.com/#PG2524A1) use naming `[year]_[type]_[name]` - MPM
* [ ]  Edit flows for smart campaign - MPM
* [ ]  Update tokens with 3 bullets of landing page - MPM
* [ ]  Add # as link url until Pathfactory steps below are complete - MPM

## Create campaign in Salesforce
* [ ]  In Marketo program, under "salesforce campaign sync" choose "create new" and include link to epic - -MPM
* [ ]  Find the [campaign in SFDC](https://gitlab.my.salesforce.com/701?fcf=00B61000004NYQP&rolodexIndex=-1&page=1) and update the start date to be the launch date and end date 1 year later - MPM
* [ ]  Update SFDC campaign owner to be MPM Owner - MPM

## Add to PathFactory
* [ ]  Add asset to content track - [follow uploading instructions in handbook](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/) - MPM
* [ ]  If adding form strategy, ensure `Show to known visitors` is toggled `OFF` - [see instructions for using form strategy in PathFactory](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/)
* [ ]  Update tokens in Marketo with PathFactory link (do not include email tracking link) - MPM
* [ ]  Update URL in WIP landing page MR with PathFactory link (at URL closing, include `&lb_email=` *without* Marketo email token) - MPM
* [ ]  Document all changes and updates and link issue in [PathFactory changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit?usp=sharing) - MPM

## Set up listening campaign in Marketo
* [ ]  Set up listening campaign in Marketo by cloning [this program template](https://app-ab13.marketo.com/#PG3875A1) - MPM
* [ ]  Comment in issue when complete and log to [PathFactory changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - MPM
* [ ]  Ping @sdaily in issue for review

## Test and push live
* [ ]  Test pipeline-approved page - MPM
* [ ]  Merge MR - MPM
* [ ]  Test fully functioning page live - MPM
* [ ]  Notify requester in issue comments that page is live and traffic can be pushed there - MPM
* [ ]  Add new gated page to resource page [gated content handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#8%EF%B8%8F%E2%83%A3-add-your-page-to-the-rsesources-page) - MPM
* [ ]  Update [#content-updates Slack channel](https://gitlab.slack.com/messages/C90CPFE2W) with gated link and Pathfactory link - MPM

Questions refer to [handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-programs/)

/label ~"Marketing Campaign" ~"Marketing Programs" ~"MktgOps - FYI" ~"MPM - Landing Page & Design" ~"mktg-status::wip" ~"PathFactory"