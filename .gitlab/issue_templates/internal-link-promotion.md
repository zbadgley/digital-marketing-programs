## Internal link promotion request

* **Page** What page(s) do we need to promote within about.gitlab.com?
* **Topic** What topic does the promoted page address?
* **Intent:** What's the stage of the buyer journey for visitors to this page?
 - Awareness (What is...?)
 - Consideration (GitLab CI)
 - Purchase (GitLab CI Demo)
* **When do you need this research completed?** Please let us know when you need this research by setting a due date or making a note here. 


/assign @shanerice
/label ~"Digital Marketing Programs" ~"mktg-status::wip" ~"SEO"