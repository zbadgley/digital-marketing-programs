Please select a request below and fill out all the information under your selection. 

**Option #1 - New heatmap**

- [ ]  Create a heatmap
  - **URL:** `e.g. https://about.gitlab.com/free-trial`
  - [ ] Exclude elements from heatmap 
        -  **Elements to exclude:** (e.g. specific link clicks like `Sign In` or `Register`)  

**Option #2 - Heatmap results (paste all screenshots below)**

- [ ]  Send heatmap results
  - **URL:** `e.g. https://about.gitlab.com/free-trial`

/label ~"Digital Marketing Programs" ~"Hotjar" ~"mktg-status::plan"
/assign @shanerice
