## Purpose

This issue will track the creation of the onsite registration form & landing page for field events that don't have badge scanners onsite.

**Below to be completed by Marketing Program Manager with support of Marketing Ops**

**PLANNED LIVE DATE:** `X`

## Create and test page in Marketo
* [ ]  Update landing page URL of: `No Lead Scanner - Onsite Reg`
* [ ]  Update URL page on `03 No Lead Scanner - Onsite Event Reg` Campaign
* [ ]  Activate campaign: `03 No Lead Scanner - Onsite Event Reg` 
* [ ]  Test form with a test lead
* [ ]  Notify event organizer when the page is ready to be used
* [ ]  Notify Marketing Ops to deactivate post-event @jamesone

Please submit any questions about this template to the [#marketing_programs slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

/label ~"Marketing Programs" ~"mktg-status::wip" 