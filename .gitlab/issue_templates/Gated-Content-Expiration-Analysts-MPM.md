## Purpose
To plan for and expire/extend analyst assets as they near the end of their reprint rights. Analyst Relations (AR) will create issue and answer description details. The issue should be created once the gated asset issue is closed so that full details can be provided below. 

[Original Gating Issue](Add link here)

**Expiration Date:** `Month Date Year`

## Analyst Asset Type:
Choose one of the below asset types that best fits the expiring/extended asset
* [ ] Thought Leadership: Market focused and educational content from analyst (This asset will expire or extend, but NOT be replaced by new version)
* [ ] Vendor Comparison: Competitive assets like Gartner MQ or Forrester Wave

## Expire or Extend?
* [ ]  Expire Analyst Asset - Delete gated page and all references of the analyst asset throughout the website and handbook. (New MQs/Waves will go through [gated-content-request-Analysts-MPM](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#), not extended)
* [ ]  Extend Analyst Assert - Contract has been extended and a new link will be provided by vendor on `date`. 
*If the extended asset link is not available before the original asset expiration date, please check both boxes and comment that there will be a gap in expiring/extending. On a case by case basis MPM will either remove the old, and create extension as new, or set up a temporary redirect, depending on page traffic.*

## Where does the asset live:
* [ ] /resources/ page - @zbadgley to add link
* [ ] /analyst/ page - @rragozzine or @tompsett to add description of where it's found (highlight section, list section, etc.)
* [ ] dedicated page - @rragozzine or @tompsett to add link to dedicated page
* 

## Action Items:
* [ ]  Remove where used in ads - @mnguyen4
* [ ]  Notify #marketing-program slack of asset expiration for campaign purposes- @zbadgley
* [ ]  Report where gated page and direct asset link are being used on webpages, CTAs, and handbook (decision to redirect, remove CTAs, etc. depends on this step) - @shanerice
* [ ]  Remove from nurture where used - @aoetama
* [ ]  Remove links from website, CTAs (if asset is not extending) - @shanerice
* [ ]  Redirect links to /analyst/ page - @shanerice
* [ ]  Remove links from handbook (slack the proper group and request handbook update) - @zbadgley
* [ ]  Delete /resources/ page @zbadgley
* [ ]  Delete references/links to asset on /analysts/ page - @rragozzine
* [ ]  Add `expired` in front of Marketo/SFDC name (ex: expired_2019_gartner-report) @zbadgley
* [ ]  Turn off Marketo smart campaign for form fill AND PF Listening campaign - @zbadgley
* [ ]  Remove from PathFactory content library - @sdaily
* [ ]  Update PathFactory change log - @sdaily
* [ ]  Update sales gdoc (https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit?ts=5e5d5328#gid=381647468) @zbadgley
* [ ]  Update Analyst Content doc (https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1491223980) @zbadgley
* [ ]  Notify sales not to use it if they are using it - @rragozzine or @Tompsett
* [ ]  Remove visible googledrive link - so that people can't reference it - @rragozzine or @Tompsett

/confidential
/label ~"mktg-status::wip" ~"Marketing Programs" ~"Digital Marketing Programs" ~"Analyst Relations" ~"PathFactory" ~"Asset Expiration"

/assign @zbadgley @shanerice @jgragnola @sdaily @tompsett @rragozzine 