## Keyword research request

* **Topic** What topic do we need to research?
* **Intent:** What's the stage of the buyer journey for visitors to this page?
 - Awareness (What is...?)
 - Consideration (GitLab CI)
 - Purchase (GitLab CI Demo)
* **When do you need this research completed?** Please let us know when you need this research by setting a due date or making a note here. 


/assign @shanerice @ncregan
/label ~"Digital Marketing Programs" ~"mktg-status::wip" ~"SEO"