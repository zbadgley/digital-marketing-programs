## Purpose

To suggest a marketing campaign idea to the Marketing Programs team. These will be reviewed and discussed in how they fit into our calendar of upcoming campaigns and based on goals of the team and overall marketing team.

## Basic concept and goals

`Summarize the overall concept of your campaign. What is the goal of the campaign?`

## Audience

`Share the function (i.e. security, devops) and seniority (i.e. individual contributors, managers, C-level) as well as any other details you feel are relevant (i.e. users of a specific technology). If there is a geographic region, please indicate as well. Be as specific as necesary!`

## Preliminary research

`Please include any relevant links or commentary as to why this should be a focus in our Marketing programs horizon.`

## Timing

`Is there a specific event or timing for which this campaign is time-based? If not, please indicate as such.`

/label ~"Marketing Programs" ~"Digital Marketing Programs" ~"Marketing Campaign" ~"mktg-status::plan"

/assign @jgragnola