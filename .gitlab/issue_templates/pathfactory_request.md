## Purpose

This issue will track the request for author-level PathFactory requests. Please provide as much information as possible.

## ✔️ Check the appropriate request below

Remove other options that were not checked and provide a brief overview of the specific changes being made.

* [ ]  Create new target track
* [ ]  Create new recommended track
* [ ]  Add new URL to existing target track
* [ ]  Add new URL to exisiting recommended track
* [ ]  Adjustments to `[LIVE]` track 
* [ ]  Add a website promoter to a page
* [ ]  Adjust live website promoter
* [ ]  Add form strategy to track
* [ ]  Adjust form strategy on existing track

## 📝 Please fill out the description information for the request

## For `Author` role

* [ ]  Create new track
* [ ]  Add to existing track 
* [ ]  Add new asset to existing track
* [ ]  Adjust assets in existing track
* [ ]  Determine form strategy
* [ ]  Add changes and link issue in [PathFactory changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit?usp=sharing)
* [ ]  Ping @sdaily for review
* [ ]  Test and go `[LIVE]`

/cc @sdaily
/label ~PathFactory ~"mktg-status::plan" 
