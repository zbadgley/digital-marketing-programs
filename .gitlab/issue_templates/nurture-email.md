* **Subject:**
* **CTA: [[type] title](pathfactory link)**

[add email screenshot]

`pathfactorylink&lb_email={{lead.Email Address}}` 

## Execution Progress

### [Email Copy](link to copy googledoc)

### Action Items
* [ ]  Write copy for email - Content/MPM
* [ ]  Provide edits in Copy Document - MPM
* [ ]  Set up email in Marketo nurture - MPM
* [ ]  Test email and make any edits - MPM
* [ ]  Test email final - MPM/MPM QA Partner
* [ ]  Add to nurture stream - MPM
* [ ]  Activate content in stream - MPM
* [ ]  Test in nurture stream - MPM

/confidential

/label ~"Marketing Programs" ~"Email Nurture" ~"mktg-status::wip" 