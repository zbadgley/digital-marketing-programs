# Verify tags in new campaign

When we launch new campaigns we need to verify all new components are tracked as expected. Please provide the following details so the Digital Marketing team can verify we're collecting the data we need.

**Campaign:**  
**Link to epic/issue:**  
**New page(s):**  

/assign @shanerice

/label ~"Digital Marketing Programs" ~"mktg-status::wip" ~"web analytics"