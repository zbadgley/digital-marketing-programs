# Digital Marketing Promotion Requests
If you are requesting digital marketing support for a campaign, event, webcast, new whitepaper, paid sponsorship, display ads, syndication, etc, please fill out the details below. We need at least 2 weeks to prepare most display advertising, paid search, and/or paid social campaigns because of ad design and agency turnaround time.

### Digital Campaign Brief
- Link to related Issue/Epic: `Enter URL`
- If no related Issue/Epic, please make a copy and fill out [Campaign Brief](https://docs.google.com/document/d/1Ba6jBhaJp-2TmAhr7HKDzQVGyNLO3D-t7TV4Wk21RlU/edit?usp=sharing): `Enter URL of campaign brief`

Please provide necessary information not included in related issue/epic in order to launch a digital campaign. If you don't know all the details, fill in as much as you can:

- Campaign Name: 
- Campaign Budget: 
- Campaign Description: 
- Campaign Dates: 
- Campaign Goal: `Registrations, page views, downloads, etc`
- Campaign Target Audience: `Describe audience you'd like to reach & engage`
- Campaign Creative Asset: `Enter URL(s) to GitLab creative asset(s) to use or URL to the design request issue`
   - This may require a separate ask for Design team, please make request in Corporate project and factor into timeline
   - [Here](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#digital-campaign-design-specs) is a reference sheet for digital campagin design specifications.
   - Search [creative repository](https://gitlab.com/gitlab-com/marketing/corporate-marketing/tree/master/design) if you want to use existing creative
- Note: We require a 15% agency fee for all budgets. See how your total budget is broken down with the [Budget Breakdown Calculator](https://docs.google.com/spreadsheets/d/1FIFD-wO8Ib9yHyRBnfMCxsdyHYoJtd4Dw3PJugKKgkQ/edit#gid=0)
- Note: If you need recommendations on budget, please use the [Budget Recommendation Calculator](https://docs.google.com/spreadsheets/d/1FIFD-wO8Ib9yHyRBnfMCxsdyHYoJtd4Dw3PJugKKgkQ/edit#gid=1509409312)



### Targeting

DMPs will pull targeting criteria from related issue/epic.
Need help with targeting ideas? Review this [list of targeting options](https://docs.google.com/spreadsheets/d/1ZuzFj-R7uPng76hI3XFx0kclR_uFIY5hcg0ikTZk4KM/edit?usp=sharing).

- Geo-location(s) to target (if any): `Enter geo-location(s) to target (i.e. cities, states, countries, etc.)`
- Additional Targeting: `If more granular targeting is preferred, list here if not already in related issue/epic`


### Digital Campaign Type
Please select your preference, or leave blank for DMP recommendation. Learn more about each campaign type [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#program-definitions).
- [ ] Paid Search
- [ ] Paid Social
- [ ] Paid Display
- [ ] Paid Sponsorship
- [ ] Content Syndication



### Digital Marketing team actions

- [ ] Tag events, but first determine what events we need to track based on primary CTA
- [ ] Set-up a/b test [or delete this if there is none]
- [ ] Create UTM codes and share with team


/assign @mnguyen4 and @lstinson

/label ~"Digital Marketing Programs" ~"mktg-status::plan"

