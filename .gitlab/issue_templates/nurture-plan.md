## Purpose
The purpose of this issue is to document the nurture plan for [campaign name]. Nurtures will cover our buyer journey stages for gated content, including awareness, consideration, and purchase/decision. These will build into the Marketo streams and logic to provide the right content at the right time.

The journey stages should align to the Content Journey Map created by content marketing in a previous step of the integrated campaign.

### Quick Links
* [All copy](https://docs.google.com/document/d/1-gPx00TQ2Ni67lYTiQgZKRHNBMaTFslFdxjAXGhf3zA/edit#) `MPM to clone this template and then remove this note`
* [PF link]() link when created, not during planning stage, but when PF created 
* [Marketo program]() link when created, not during planning stage, but when program created 

[Buyer Stages in Handbook]()

## Awareness
* [ ] [[type] Content Name](link to issue when created) (main asset)
* [ ] [[type] Content Name](link to issue when created)

## Consideration
* [ ] [[type] Content Name](link to issue when created)
* [ ] [[type] Content Name](link to issue when created)

## Purchase
* [ ] [[type] Content Name](link to issue when created)
* [ ] [[type] Content Name](link to issue when created)

Note: [Following thoughts here for stages - setting us up to make more sophisticated in future](https://nation.marketo.com/thread/43575-transitioning-exhausted-leads-into-new-stream#comment-198797)

/label ~"Marketing Programs" ~"Digital Marketing Programs" ~"mktg-status::wip"