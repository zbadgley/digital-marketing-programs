## Overview
* This issue should be created in planning the launch of an integrated campaign.
* The MPM will clone the template linked below and then include the link in this issue.
* The due date should be the due date of the campaign brief, based on the [Integrated Campaign GANTT Template](https://docs.google.com/spreadsheets/d/1VTrWNX9qfY99b2TnrX93P39aXiRoNnChB6tduTvmysA/edit#gid=1426779885) with the launch date designated, waterfalling down through the sheet when each action item is due.
* The MPM, DMP, and PMM DRIs for the campaign should begin this brief during the kickoff call for the integrated campaign.

### [Integrated Campaign Brief Template to Clone](https://docs.google.com/document/d/1Ba6jBhaJp-2TmAhr7HKDzQVGyNLO3D-t7TV4Wk21RlU/edit)
*Swap this link out with the new document once cloned.*

### [Integrated Campaign Meeting Notes Template to Clone](https://docs.google.com/document/d/1cKuYd1G1obcFehu6vbQKAqzTvLkfFtOZ_sB2tEc9T20/edit#)
*Swap this link out with the new document once cloned.*

/label ~"Marketing Programs" ~"Digital Marketing Programs" ~"mktg-status::wip"