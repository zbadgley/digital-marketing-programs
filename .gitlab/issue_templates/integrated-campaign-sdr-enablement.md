`Name this issue: `SDR Enablement Prep - Campaign Name`. The purpose of this issue is to prep the resources and conversation for the SDR enablement epic and training.`

There will be two parts to the SDR Enablement session. One will cover what the campaign entails (MPM), and one will be focused on the persona, positioning, and messaging (Strategic Marketing), with time allowed for questions.

In the enablement session, point to the SDR Enablemetn Epic only as the SSoT (single source of truth) - **this issue should only be used for prepping**.

## MPM Campaign Overview

[Link to recording when available]() - this should be added to the epic when the training takes place

* Overview of the campaign ([link to epic]())
  * Marketing channels in the mix
  * User experience from channels > landing page > autoresponder > nurture + pathfactory > outreach
* Cadence of blogs, webcasts, content planned
* How they will see these leads in their views (interesting moments that will appear in lead views)

## Strategic Marketing Enablement
* Target persona(s)
* Positioning and key messaging

## Key Resources for Prepping

* [SDR enablement epic]() - to be linked when created
* [Outreach sequences]() - to be linked when created

/label ~"Marketing Programs" ~"Strategic Marketing" ~"mktg-status::plan" ~"SDR"