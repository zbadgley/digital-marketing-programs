Name this issue: `Landing Page Copy - Campaign Name`. Delete this line after you rename the file.

## Overview

The purpose of this issue is to for MPM to collaborate with Content and PMM on the creation of the primary campaign landing page copy.

## Key Resources for Prepping

* :memo: **[Campaign brief >>]()** - to be linked when created
* :busts_in_silhouette: **[Persona / Positioning Matrix /Polished Messaging>>]()** - to be linked when created

## Deliverables/due dates
* [ ] MPM to draft landing page copy in [Campaign Brief >>]() - link to landing page section in campaign brief | **Due:** `Enter YYYY-MM-DD when this step should be complete`
* [ ] PMM to review landing page copy to make sure it aligns with campaign messaging|  **Due:** `Enter YYYY-MM-DD when this step should be complete`
* [ ] Content to review/edit landing page copy to make sure it is polished| **Due:** `Enter YYYY-MM-DD when this step should be complete`
