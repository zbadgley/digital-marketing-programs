Use this template to conduct on-page optimization for `about.gitlab.com`. Please fill in all the details and adjust DRIs accordingly.

### Workflow

- [ ] review campaign brief and align messaging on landing page
- [ ] review SEO, keyword research, and target audience language
- [ ] conversion optimization tests for copy and design on linked assets 
- [ ] landing page copy / ad copy alignment
- [ ] review PathFactory track for any adjustments (if applicable)

### Resources

/label ~"mktg-status::plan"