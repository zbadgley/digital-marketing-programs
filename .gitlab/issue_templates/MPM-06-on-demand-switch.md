## Purpose

This issue will track the process of converting a LIVE webcast to an on-demand version.

**To be completed by Marketing Program Manager:**

## :video\_camera: YouTube and Marketing Site updates

* [ ] Upload recording to YouTube - MPM (Same day post-webcast)
* [ ] Add PathFactory URL to landing page in www-gitlab-com - MPM (1 BD post-webcast)
* [ ] Add on-demand webcast to /resources page (1 BD post-webcast)

## Add to PathFactory (1 BD post-webcast)

* [ ]  Add YouTube URL to PathFactory ([follow handbook instructions](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/)) - MPM
* [ ]  Set up listening campaign in Marketo by cloning [this program template](https://app-ab13.marketo.com/#PG3875A1) - MPM
* [ ]  Comment in issue when complete and log to [PathFactory changelog](https://docs.google.com/document/d/1qd9X-V0WNBTklCKNYVRmjJtiOcPu6dZYkfJ2uuQt_Co/edit) - MPM
* [ ]  Ping @sdaily in issue for review

/label ~"Marketing Programs" ~"mktg-status::wip" ~"MPM - Switch to On-Demand" ~"PathFactory"