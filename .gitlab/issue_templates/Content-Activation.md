## Purpose

We have a new piece of content coming up... let's plan for how to activate this great content to the fullest (in nurture, in pathfactory, on the website, in blogs, etc.) This issue is to be used by Markeitng Programs to strategize where this asset best fits in our campaigns.

## About the content

**Official Name:**
**Content Stage:**
    * [ ] Awareness
    * [ ] Consideration
    * [ ] Decision/Purchase
**Use Case:**
    * [ ] SCM
    * [ ] CI
    * [ ] CD
    * [ ] DevSecOps
    * [ ] Cloud Native
    * [ ] Agile
    * [ ] Simplify DevOps
    * [ ] IAC / GitOps
    
*Include abstract about the content here*

## Activation Planning
*Not all of the below will be relevant... this is WIP iteration 1. Nurture and pathfactory are minimum, need to discuss webpage planning and blog with content.*
* [ ] Add to nurture: [name of nurture](url) - DRI
* [ ] Add to pathfactory stream: [name of stream](url) - DRI
* [ ] Add CTA on webpage: [name of webpage](url) - DRI
* [ ] Add to recent blog (or create new?): [blog](url) - DRI


## Documentation
* [ ] Update [Nurture Matrix](https://docs.google.com/spreadsheets/d/108VzbY5oISw-KSRB8903g3e7ZU6yFT-602A_GjM0kbg/edit#gid=0) to address where the resource is incorporated
