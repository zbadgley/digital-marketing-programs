## Purpose

The purpose of this issue is to determine use of the integrated campaign within the upcoming field and corporate events planning.

## Question
* What events are coming up relevant to this integrated campaign (it's persona and positioning)?
* How can we amplify the message of this integrated campaign at the upcoming events?
* Can we use the Outreach sequences from this integrated campaign for SDR motion in following up on leads from this event? Or are adjustments necessary for a seamless follow up?

## Events
*Fill in the below code with links to the relevant event issues/epics
* [(Name of Event 1) Epic]() - Owner Name & gitlab handle
* [(Name of Event 2) Epic]() - Owner Name & gitlab handle
* [(Name of Event 3) Epic]() - Owner Name & gitlab handle

/label ~"Marketing Programs" ~"Field Marketing" ~"mktg-status::wip"